package fr.siocoliniere;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.view.WelcomeForm;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        System.out.println("Lancement de l'application");

        WelcomeForm welcomeView = new WelcomeForm();
        welcomeView.setVisible(true);


        // DEBUG
        // -- récupération des hackathons

        List<Hackathon> hackathons = HackathonController.getInstance().getAll();
        for (Hackathon hacka : hackathons) {
            System.out.println(hacka);
        }


    }
}
