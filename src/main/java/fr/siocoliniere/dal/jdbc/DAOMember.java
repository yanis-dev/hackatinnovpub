package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class DAOMember
 * Implements abstract class DAO
 * BD : Table Member
 */
public class DAOMember extends DAO<Member> {

    /**
     * PATTERN SINGLETON
     */
    private static DAOMember instanceDAOMember;
    private Member obj;

    /**
     * Pattern Singleton
     * 
     * @return DAOMember
     */
    public static synchronized DAOMember getInstance() {
        if (instanceDAOMember == null) {
            instanceDAOMember = new DAOMember();
        }
        return instanceDAOMember;
    }

    /**
     * REQUESTS
     */
    private static final String sqlSelectOneById = "select member.id,member.firstname, member.lastname from member where member.id = ?";
    private static final String sqlSelectAllParticipationJuryByIdHackathon = "select member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.hackathonid = ? and participation.roleid = 1";
    private static final String sqlSelectAllNonJuryByIdHackathon = "select member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.hackathonid = ? and participation.roleid != 2";
    private static final String sqlSelectAllAnimsByHackathon = "select member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.hackathonid = ? and participation.roleid = 3";
    private static final String sqlSelectAllNonAnimByIdHackathon = "select member.id, member.lastname, member.firstname from member inner join participation on member.id = participation.memberid where participation.hackathonid = ? and participation.roleid != 3";

    private static final String sqlUpdateMember = "update member set firstname=?, lastname=? where id=?";

    /**
     * Get all members
     * 
     * @return
     * @throws DALException
     */
    @Override
    public List<Member> getAll() throws DALException {
        // TO IMPLEMENT
        return null;
    }

    /**
     * Retrieves the member whose id is passed as a parameter, with the role "jury"
     * (roleid = 1)
     * 
     * @param id id of member
     * @return the member
     * @throws DALException
     */
    @Override
    public Member getOneById(int id) throws DALException {
        Member member = null;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                member = new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname"));
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        return member;
    }

    /**
     * Retrieves all the members who participated in the hackathon whose id is
     * passed as a parameter, with the role "jury" (roleid = 1)
     * 
     * @param hackid id of hackathon
     * @return list of members
     * @throws DALException
     */
    public List<Member> getAllJuryByIdHackathon(int hackid) throws DALException {

        List<Member> juries = new ArrayList<>();

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectAllParticipationJuryByIdHackathon);
            rqt.setInt(1, hackid);
            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                juries.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname")));
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        return juries;
    }

    public List<Member> getAllNonJuryByIdHackathon(int hackid) {
        List<Member> members = new ArrayList<Member>();

        try {
            var cnx = JdbcTools.getConnection();
            var stmt = cnx.prepareStatement(sqlSelectAllNonJuryByIdHackathon);
            stmt.setInt(1, hackid);
            var rs = stmt.executeQuery();
            while (rs.next()) {
                members.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname")));
            }
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return members;
    }

    public List<Member> getAllAnimsByHackathon(int hackid) {

        List<Member> members = new ArrayList<Member>();

        try {
            var cnx = JdbcTools.getConnection();
            var stmt = cnx.prepareStatement(sqlSelectAllAnimsByHackathon);
            stmt.setInt(1, hackid);
            var rs = stmt.executeQuery();
            while (rs.next()) {
                members.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname")));
            }
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return members;
    }

    public List<Member> getAllNonAnimsByIdHackathon(int hackid) {
        List<Member> members = new ArrayList<Member>();

        try {
            var cnx = JdbcTools.getConnection();
            var stmt = cnx.prepareStatement(sqlSelectAllNonAnimByIdHackathon);
            stmt.setInt(1, hackid);
            var rs = stmt.executeQuery();
            while (rs.next()) {
                members.add(new Member(rs.getInt("id"), rs.getString("lastname"), rs.getString("firstname")));
            }
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }
        return members;
    }

    @Override
    public void save(Member obj) throws DALException {
        // TO IMPLEMENT
    }

    @Override
    public void update(Member obj) {
        try {
            var conn = JdbcTools.getConnection();
            var stmt = conn.prepareStatement(sqlUpdateMember);
            stmt.setString(1, obj.getFirstName());
            stmt.setString(2, obj.getLastName());
            stmt.setInt(3, obj.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
