package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.utils.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcTools {

    private static String urldb;
    private static String userdb;
    private static String passworddb;
    private static Connection connection = null;

    public static Connection getConnection() throws SQLException {

        if(connection==null) {
            urldb = Config.getInstance().getProperty("db.url");
            userdb = Config.getInstance().getProperty("db.username");
            passworddb = Config.getInstance().getProperty("db.password");
            connection = DriverManager.getConnection(urldb, userdb, passworddb);
        }
        return connection;
    }
}


