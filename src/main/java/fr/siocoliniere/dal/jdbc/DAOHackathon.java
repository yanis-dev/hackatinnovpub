package fr.siocoliniere.dal.jdbc;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.DAO;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class DAOHackathon
 * Implements abstract class DAO
 * BD : Table Hackathon
 */
public class DAOHackathon extends DAO<Hackathon> {

    /**
     * PATTERN SINGLETON
     */
    private static DAOHackathon instanceDAOHackathon;

    /**
     * Pattern Singleton
     * 
     * @return DAOHackathon
     */
    public static synchronized DAOHackathon getInstance() {
        if (instanceDAOHackathon == null) {
            instanceDAOHackathon = new DAOHackathon();
        }
        return instanceDAOHackathon;
    }

    /**
     * REQUETES
     */

    private static final String sqlInsert = "UPDATE hackathon SET name = ?, topic = ?, description = ?, date = ?, lieu = ?, theme = ?, registration = ? WHERE id = CAST(? AS INTEGER)";

    private static final String sqlSelectIdByName = "select hackathon.id from hackathon where hackathon.name  = ?";
    private static final String sqlSelectAll = "select * from hackathon";
    private static final String sqlSelectOneById = "select * from hackathon where hackathon.id = ?";
    private static final String sqlUpdate = "insert into hackathon(name, topic, description,date, lieu, theme, registration) VALUES(?,?,?,?,?,?,?)";

    /**
     * Get all hackathons
     * 
     * @return list of hackathons
     * @throws DALException
     */
    @Override
    public List<Hackathon> getAll() throws DALException {

        List<Hackathon> hackathonList = new ArrayList<>();

        List<Member> memberList;
        List<Member> animsList;
        try {
            Connection cnx = JdbcTools.getConnection();
            Statement rqt = cnx.createStatement();
            ResultSet rs = rqt.executeQuery(sqlSelectAll);

            while (rs.next()) {
                // hackathon's instanciation
                Hackathon hacka = new Hackathon(rs.getInt("id"), rs.getString("name"), rs.getString("topic"),
                        rs.getString("description"), LocalDate.parse(rs.getDate("date").toString()),
                        rs.getString("lieu"), rs.getString("theme"), rs.getString("registration"));

                // gets members of the hackathon
                memberList = DAOMember.getInstance().getAllJuryByIdHackathon(hacka.getId());
                animsList = DAOMember.getInstance().getAllAnimsByHackathon(hacka.getId());
                hacka.setJuryMembers(memberList);
                hacka.setAnimsMembers(animsList);
                hackathonList.add(hacka);
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }

        return hackathonList;
    }

    /**
     * Retrieves the ID of a hackathon knowing the name (constraint UNIQUE on the
     * name)
     * 
     * @param name : name of hackathon sought
     * @return id of hackathon
     * @throws DALException
     */
    public Integer getIdByName(String name) throws DALException {

        Integer hackid = null;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectIdByName);
            rqt.setString(1, name);
            ResultSet rs = rqt.executeQuery();
            while (rs.next()) {
                // instanciation du hackathon
                hackid = rs.getInt("id");
            }
            rqt.close();

        } catch (SQLException e) {
            // Exception personnalisée
            throw new DALException(e.getMessage(), e);
        }
        return hackid;
    }

    /**
     * Retrieves a hackathon from its ID
     * 
     * @param id of hackathon
     * @return the hackathon whose id is passed as a parameter
     * @throws DALException
     */
    @Override
    public Hackathon getOneById(int id) throws DALException {
        Hackathon hackat = null;

        List<Member> memberList;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
            rqt.setInt(1, id);

            ResultSet rs = rqt.executeQuery();

            while (rs.next()) {
                hackat = new Hackathon(rs.getInt("id"), rs.getString("name"), rs.getString("topic"),
                        rs.getString("description"), LocalDate.parse(rs.getString("date")),
                        rs.getString("lieu"), rs.getString("theme"), rs.getString("registration"));

                // gets members of the hackathon
                memberList = DAOMember.getInstance().getAllJuryByIdHackathon(rs.getInt("id"));
                hackat.setJuryMembers(memberList);
            }
            rqt.close();

        } catch (SQLException e) {
            throw new DALException(e.getMessage(), e);
        }
        return hackat;
    }

    public void insert(Hackathon obj) throws DALException {
        Hackathon hackathon = obj;

        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlUpdate);
            rqt.setString(1, hackathon.getName());
            rqt.setString(2, hackathon.getTopic());
            rqt.setString(3, hackathon.getDescription());
            rqt.setDate(4, Date.valueOf(hackathon.getDate()));
            rqt.setString(5, hackathon.getLieu());
            rqt.setString(6, hackathon.getTheme());
            rqt.setString(7, hackathon.getRegistration());

            rqt.executeUpdate();
            rqt.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void update(Hackathon obj) throws DALException {
        // TO IMPLEMENT
        Hackathon hackathon = obj;
        try {
            Connection cnx = JdbcTools.getConnection();
            PreparedStatement rqt = cnx.prepareStatement(sqlInsert);
            rqt.setString(1, hackathon.getName());
            rqt.setString(2, hackathon.getTopic());
            rqt.setString(3, hackathon.getDescription());
            // convert the date to java.sql.Date
            java.sql.Date sqlDate = java.sql.Date.valueOf(hackathon.getDate());
            rqt.setDate(4, sqlDate);
            rqt.setString(5, hackathon.getLieu());
            rqt.setString(6, hackathon.getTheme());
            rqt.setString(7, hackathon.getRegistration());
            rqt.setInt(8, hackathon.getId());
            System.out.println("updated bdd");
            rqt.executeUpdate();
            rqt.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void save(Hackathon hackathon) {
    }

    // public Hackathon getById(int id) {
    // Hackathon hackathon = null;
    // try {
    // Connection cnx = JdbcTools.getConnection();
    // PreparedStatement rqt = cnx.prepareStatement(sqlSelectOneById);
    // rqt.setInt(1, id);
    // ResultSet rs = rqt.executeQuery();

    // if (rs.next()) {
    // int hackathonId = rs.getInt("id");
    // String name = rs.getString("name");
    // String topic = rs.getString("topic");
    // String description = rs.getString("description");

    // hackathon = new Hackathon(hackathonId, name, topic, description);
    // }
    // rqt.close();
    // } catch (SQLException e) {
    // System.out.println(e);
    // }
    // return hackathon;
    // }

    public void updateParticipationToJury(int hackathonId, int memberId) {
        try {
            Connection conn = JdbcTools.getConnection();
            PreparedStatement rqt = conn.prepareStatement(
                    "update participation set roleid=(select id from role where name='jury') where hackathonid=? and memberid=?");
            rqt.setInt(1, hackathonId);
            rqt.setInt(2, memberId);
            rqt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void deleteJuryMember(int hackathonId, int memberId) {
        try {
            Connection conn = JdbcTools.getConnection();
            PreparedStatement rqt = conn.prepareStatement(
                    "delete from participation where memberid=? and hackathonid=?");
            rqt.setInt(2, hackathonId);
            rqt.setInt(1, memberId);
            rqt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateParticipationToAnim(int hackathonId, int memberId) {
        try {
            Connection conn = JdbcTools.getConnection();
            PreparedStatement rqt = conn.prepareStatement(
                    "update participation set roleid=(select id from role where name='animateur') where hackathonid=? and memberid=?");
            rqt.setInt(1, hackathonId);
            rqt.setInt(2, memberId);
            rqt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateParticipationToMember(int hackathonId, int memberId) {
        try {
            Connection conn = JdbcTools.getConnection();
            PreparedStatement rqt = conn.prepareStatement(
                    "update participation set roleid=(select id from role where name='inscrit') where hackathonid=? and memberid=?");
            rqt.setInt(1, hackathonId);
            rqt.setInt(2, memberId);
            rqt.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
