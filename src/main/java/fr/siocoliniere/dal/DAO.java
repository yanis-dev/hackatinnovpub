package fr.siocoliniere.dal;

import java.util.List;

/**
 * Class DAO
 *
 * Pattern DAO (Data Access Object) : allows you to make the link between the
 * business layer (bo) and the persistent layer (dal)
 * in order to centralize the mapping mechanisms between our storage system and
 * our Java objects.
 *
 * Generic class type <T> : implemented to create DAO for differents type of
 * object
 *
 * @param <T>
 */
public abstract class DAO<T> {

    public abstract List<T> getAll() throws DALException;

    public abstract T getOneById(int id) throws DALException;

    public abstract void save(T obj) throws DALException;

    public abstract void update(T obj) throws DALException;

}
