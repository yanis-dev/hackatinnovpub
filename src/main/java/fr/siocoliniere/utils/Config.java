package fr.siocoliniere.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;
public class Config {

    private static Config cfg = null;
    private Properties props;
    public static synchronized Config getInstance() {
        if (Config.cfg == null) {
            Config.cfg = new Config();
        }
        return Config.cfg;
    }
    private Config() {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        props = new Properties();
        InputStream resourceStream = null;
        Connection connection = null;

        try {
            // Opening .properties file
            resourceStream = loader.getResourceAsStream("db.properties");
            props.load(resourceStream);

        } catch (final IOException ex) {
            ex.printStackTrace();
        } finally {
            if (resourceStream != null) {
                try {
                    resourceStream.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }
}
