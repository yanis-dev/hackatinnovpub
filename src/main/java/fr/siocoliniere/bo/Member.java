package fr.siocoliniere.bo;

import java.util.Objects;

/**
 * Classe Member
 */
public class Member {
    private int id;
    private String lastname;
    private String firstname;

    /**
     * Constructor
     * @param lastname member's lastname
     * @param firstname member's firstname
     */
    public Member(int id, String lastname, String firstname) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return id == member.id && Objects.equals(lastname, member.lastname) && Objects.equals(firstname, member.firstname);
    }

    public int getId() {
        return id;
    }

    public String getFirstName(){
        return firstname;
    }

    public String getLastName(){
        return lastname;
    }

    public void setFirstName(String name){
        firstname=name;
    }

    public void setLastName(String name){
        lastname=name;
    }
    @Override
    public String toString() {
        return  lastname + ' ' + firstname ;
    }
}
