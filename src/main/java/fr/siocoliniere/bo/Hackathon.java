package fr.siocoliniere.bo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Hackathon
 */
public class Hackathon {

    private int id;
    private String name;
    private String topic;
    private String description;
    private String lieu;
    private String theme;
    private LocalDate date;
    private String registration;
    private List<Member> juryMembers; // List of jury members
    private List<Member> animsMembers; // List of animators members

    /**
     * Constructor
     * 
     *
     * @param topic hackathon's topic
     * @param desc  hackathon's description
     */
    public Hackathon(String name, String topic, String desc, LocalDate date, String lieu, String theme,
            String registration) {
        this.id = 0;
        this.name = name;
        this.topic = topic;
        this.description = desc;
        this.date = date;
        this.lieu = lieu;
        this.theme = theme;
        this.registration = registration;

        this.juryMembers = new ArrayList<>();
        this.animsMembers = new ArrayList<>();
    }

    /**
     * Contructor
     * 
     * @param id    hackathon's id
     * @param topic hackathon's topic
     * @param desc  hackathon's description
     */
    public Hackathon(int id, String name, String topic, String desc, LocalDate date, String lieu, String theme,
            String registration) {
        this(name, topic, desc, date, lieu, theme, registration);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public String getTopic() {
        return this.topic;
    }

    public String getDescription() {
        return this.description;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public String getLieu() {
        return this.lieu;
    }

    public String getTheme() {
        return this.theme;
    }

    public String getRegistration() {
        return this.registration;
    }

    public List<Member> getJuryMembers() {
        return juryMembers;
    }

    public List<Member> getAnimsMembers() {
        return animsMembers;
    }

    public void setJuryMembers(List<Member> juryMembers) {
        this.juryMembers = juryMembers;
    }

    public void setAnimsMembers(List<Member> animsMembers) {
        this.animsMembers = animsMembers;
    }

    /**
     * Adds a member to the list of jury members
     * 
     * @param member new member
     * @return true if member has been added, false otherwise
     */
    public boolean addJuryMember(Member member) {
        boolean add = false;
        if (!juryMembers.contains(member)) {
            juryMembers.add(member);
            add = true;
        }
        return add;
    }

    public boolean addAnimMember(Member member) {
        boolean add = false;
        if (!animsMembers.contains(member)) {
            animsMembers.add(member);
            add = true;
        }
        return add;
    }

    @Override
    public String toString() {
        return this.name + " - " + this.topic;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(LocalDate date){this.date = date;}

    public void setLieu(String lieu){this.lieu = lieu;}

    public void setTheme(String theme){ this.theme = theme;}

    public void setRegistration(String registration){this.registration = registration;}

}
