package fr.siocoliniere.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

public class ManageAnim extends JFrame {

    private JPanel pnl_main;
    private JPanel pnl_manage;
    private JPanel pnl_right;
    private JPanel pnl_left;
    private static JList<Member> lst_anim;
    private JScrollPane scr_animList;
    private JButton btn_add;
    private JButton btn_delete;
    private Hackathon hackathon;
    private static ManageAnim instanceManageAnim;
    private DefaultListModel<Member> animModel;

    public ManageAnim() {

        hackathon = WelcomeForm.toAnotherWin();
        instanceManageAnim = ManageAnim.this;
        pnl_main = new JPanel();
        pnl_manage = new JPanel();
        pnl_right = new JPanel();
        pnl_left = new JPanel();
        lst_anim = new JList<Member>();
        animModel = new DefaultListModel<Member>();
        var anims = HackathonController.getInstance().getAnimsByHackathon(hackathon.getId());
        for (int i = 0; i < anims.size(); i++) {
            animModel.addElement(anims.get(i));
        }
        lst_anim.setModel(animModel);
        scr_animList = new JScrollPane(lst_anim);
        btn_add = new JButton();
        btn_delete = new JButton();

        // this
        var contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        // pnl_manage
        {
            pnl_manage.setLayout(new GridLayout(1, 2));
            pnl_manage.setBackground(new Color(120, 134, 98));

            // pnl_right
            {
                pnl_right.setLayout(new FlowLayout());

                // src_juryList
                scr_animList.setPreferredSize(new Dimension(200, 150));
                pnl_right.add(scr_animList);

                pnl_manage.add(pnl_right);
            }

            // pnl_left
            {
                pnl_left.setLayout(new GridLayout(2, 1));

                // btn_add
                btn_add.setText("Add");
                btn_add.setMaximumSize(new Dimension(50, 30));
                btn_add.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new AddAnimView().setVisible(true);
                    }

                });
                pnl_left.add(btn_add);

                // btn_delete
                btn_delete.setText("Delete");
                btn_delete.setMaximumSize(new Dimension(50, 30));
                btn_delete.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (lst_anim.getSelectedValue() != null) {
                            HackathonController.getInstance().DeleteAnimMember(hackathon, lst_anim.getSelectedValue());
                            ManageAnim.this.setVisible(false);
                            new ManageAnim().setVisible(true);
                        } else {
                            System.out.println("error");
                            JOptionPane.showMessageDialog(null, "Please select an animator to delete");

                        }

                    }

                });
                pnl_left.add(btn_delete);

                pnl_manage.add(pnl_left);
                pnl_manage.setBackground(new Color(120, 200, 168));
            }
        }
        contentPane.add(pnl_manage);
        contentPane.add(pnl_main);
        pack();

        setTitle("Manage animators");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocationRelativeTo(null);
        setPreferredSize(new Dimension(300, 150));

    }

    public static ManageAnim getInstance() {
        if (instanceManageAnim == null) {
            instanceManageAnim = new ManageAnim();
        }
        return instanceManageAnim;
    }

}
