package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;

import java.time.LocalDate;

import java.util.Vector;

import javax.swing.*;

import com.github.lgooddatepicker.components.DatePicker;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.custom.TextFieldCustom;

public class AddHackathon extends JFrame {

    TextFieldCustom txt_name;
    TextFieldCustom txt_topic;
    TextFieldCustom txt_description;
    TextFieldCustom txt_lieu;
    TextFieldCustom txt_theme;
    JPanel pnl_add;
    JPanel pnl_bottom;
    JButton btn_add;
    private JButton btn_close;
    DatePicker datePicker;
    JComboBox cmb_registration;

    public AddHackathon() {
        super("Add Hackathon");
        setSize(500, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(false);

        txt_name = new TextFieldCustom("Nom");
        txt_topic = new TextFieldCustom("Topic");
        txt_description = new TextFieldCustom("Description");
        pnl_add = new JPanel();
        pnl_bottom = new JPanel(null);
        btn_close = new JButton();
        btn_add = new JButton();
        datePicker = new DatePicker();
        txt_lieu = new TextFieldCustom("Lieu");
        txt_theme = new TextFieldCustom("Theme");
        cmb_registration = new JComboBox(new Vector(HackathonController.getInstance().getRegistrationTypes().values()));

        // this
        var contentPane = getContentPane();
        contentPane.setLayout(new GridLayout(2, 1));

        // ==== pnl_add ===

        {

            pnl_add.setLayout(new FlowLayout());

            // ----- txt_name -----
            txt_name.setText("");
            txt_name.setPreferredSize(new Dimension(100, 30));
            pnl_add.add(txt_name);

            // ----- txt_topic -----
            txt_topic.setPreferredSize(new Dimension(100, 30));
            pnl_add.add(txt_topic);

            // ----- txt_description -----
            txt_description.setPreferredSize(new Dimension(100, 30));
            pnl_add.add(txt_description);

            // ---- btn_add ----
            btn_add.setText("Create Hackathon");
            btn_add.setPreferredSize(new Dimension(150, 30));
            btn_add.addActionListener(this::addButtonActionperformed);
            pnl_bottom.add(btn_add);

            btn_close.setText("Close");
            btn_close.setPreferredSize(new Dimension(100, 30));
            btn_close.addActionListener(this::closeButtonActionPerformed);
            pnl_bottom.add(btn_close);

            // ----- datePicker -----
            pnl_add.add(datePicker);
            datePicker.setText("Select a date");

            // ----- txt_lieu -------
            txt_lieu.setPreferredSize(new Dimension(100, 30));
            pnl_add.add(txt_lieu);

            // ----- txt_theme
            txt_theme.setPreferredSize(new Dimension(100, 30));
            pnl_add.add(txt_theme);

            // ----- cmb-registrationTypes
            pnl_add.add(cmb_registration);

        }
        contentPane.add(pnl_add);
        // ---- pnl_bottom ----
        pnl_bottom.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 40));
        // pnl_bottom.setBackground(new Color(100, 200, 50));
        contentPane.add(pnl_bottom);

    }

    private void addButtonActionperformed(ActionEvent e) {
        String nameValue = txt_name.getText();
        String topicValue = txt_topic.getText();
        String descriptionValue = txt_description.getText();
        LocalDate dateValue = datePicker.getDate();
        String lieuValue = txt_lieu.getText();
        String themeValue = txt_theme.getText();
        String registrationValue = cmb_registration.getSelectedItem().toString();
        System.out.println(registrationValue);

        if (nameValue.equals("Nom")) {
            JOptionPane.showMessageDialog(null, "Un nom est requis");
        } else if (topicValue.equals("Topic")) {
            JOptionPane.showMessageDialog(null, "Un topic est requis");
        } else if (descriptionValue.equals("Description")) {
            JOptionPane.showMessageDialog(null, "Une description est requise");
        } else if (lieuValue.equals("Lieu")) {
            JOptionPane.showMessageDialog(null, "Un lieu est requis");
        } else if (themeValue.equals("Thele")) {
            JOptionPane.showMessageDialog(null, "Un theme est requis");
        } else if (dateValue == null) {
            JOptionPane.showMessageDialog(null, "Une date est requise");
        } else if (registrationValue == HackathonController.getInstance().getRegistrationTypes().get(0)) {
            JOptionPane.showMessageDialog(null, "Un type est requis");
        } else {
            HackathonController.getInstance().addNewHackathon(nameValue, topicValue, descriptionValue, dateValue,
                    lieuValue, themeValue, registrationValue);
            txt_name.setText("");
            txt_description.setText("");
            txt_topic.setText("");
            datePicker.setDate(null);
            txt_lieu.setText("");
            txt_theme.setText("");
            cmb_registration.setSelectedIndex(0);
            JOptionPane.showMessageDialog(null, "Hakathon successfully created !");
            System.out.println("Hackathon created");

        }

    }

    private void closeButtonActionPerformed(ActionEvent e) {
        System.out.println("close");
        super.dispose();
        new WelcomeForm().setVisible(true);
    }

    public JButton getCloseButton() {
        return btn_close;
    }

}
