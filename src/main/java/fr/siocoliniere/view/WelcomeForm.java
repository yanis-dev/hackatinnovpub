package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.*;
import java.security.SignatureException;
import java.util.List;
import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;

public class WelcomeForm extends JFrame {

    private JPanel pnl_top;
    private JPanel pnl_manage;
    private JPanel pnl_button;
    private static JComboBox<Hackathon> cmb_hackathons;
    private JButton btn_manage;
    private JButton btn_jury;
    private JLabel lbl_error;
    private JPanel pnl_window;
    private JPanel pnl_bottom;
    private JButton btn_close;
    private JButton btn_anim;
    private JComboBox<Hackathon> comboBox;
    private JButton btn_add;
    private JButton btn_experts;

    /**
     * Constructor
     */
    public WelcomeForm() {
        pnl_top = new JPanel(null);
        pnl_manage = new JPanel();
        pnl_button = new JPanel();
        cmb_hackathons = new JComboBox<Hackathon>();
        btn_manage = new JButton();
        btn_add = new JButton();
        btn_jury = new JButton();
        btn_anim = new JButton();
        lbl_error = new JLabel();
        pnl_window = new JPanel();
        pnl_bottom = new JPanel(null);
        btn_close = new JButton();
        btn_experts = new JButton();

        // pnl_top.setBackground(new Color(100, 120, 98));

        // ======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(new GridLayout(3, 1));

        // ---- pnl_top ----
        pnl_top.setPreferredSize(new Dimension(10, 20));
        contentPane.add(pnl_top);

        // ======== pnl_manage ========
        {
            pnl_manage.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 20));

            // ---- cmb_hackathons ----
            cmb_hackathons.setPreferredSize(new Dimension(300, 30));
            pnl_manage.add(cmb_hackathons);
            // ---- pnl_button ----
            pnl_button.setLayout(new BoxLayout(pnl_button, BoxLayout.Y_AXIS));
            // ---- btn_manage ----
            btn_manage.setText("Edit Hackathon");
            btn_manage.setPreferredSize(new Dimension(150, 30));
            btn_manage.addActionListener(this::manageButtonActionPerformed);
            pnl_button.add(btn_manage);

            // ---- btn_jury ----
            btn_jury.setText("Manage Jury");
            btn_jury.setPreferredSize(new Dimension(150, 30));
            btn_jury.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    new ManageJury().setVisible(true);
                }

            });
            pnl_button.add(btn_jury);
            
            //btn_jury
            btn_experts.setText("Experts");
            btn_experts.setPreferredSize(new Dimension(100,30));
            btn_experts.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    new ComposeExpertTeam().setVisible(true);
                }
                
            });
            pnl_button.add(btn_experts);
            pnl_manage.add(pnl_button);
            // ---- btn_add ----
            btn_add.setText("Create Hackathon");
            btn_add.setPreferredSize(new Dimension(150, 30));
            btn_add.addActionListener(this::addButtonActionperformed);

            pnl_button.add(btn_add);

            // ----- btn_anim -----
            btn_anim.setText("Edit animators composition");
            btn_anim.setPreferredSize(new Dimension(150, 30));
            btn_anim.addActionListener(this::animButtonActionperformed);

            pnl_button.add(btn_anim);

            // ---- lbl_error ----
            lbl_error.setText("Error Message");
            pnl_button.add(lbl_error);
        }
        contentPane.add(pnl_manage);
        pack();
        setLocationRelativeTo(getOwner());

        // ======== pnl_window ========
        {
            pnl_window.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 20));

            // ---- pnl_bottom ----
            pnl_bottom.setPreferredSize(new Dimension(300, 10));
            pnl_window.add(pnl_bottom);

            // ---- btn_close ----
            btn_close.setText("Close");
            btn_close.setPreferredSize(new Dimension(100, 30));
            btn_close.addActionListener(this::closeButtonActionPerformed);
            pnl_window.add(btn_close);
        }
        contentPane.add(pnl_window);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Hackat'Orga");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(800, 150));

        lbl_error.setVisible(false);

        initComboBoxHackathons();

    }

    /**
     * Initializes the drop-down list with existing hackathons
     */
    private void initComboBoxHackathons() {
        // !!! Info technique : un objet de type ComboBox fonctionne avec un objet de
        // type DefaultComboBoxModel qui va contenir les données

        // gets all hackathons
        List<Hackathon> hackathons = HackathonController.getInstance().getAll();

        // create Model for Combobox

        if (hackathons != null) {
            DefaultComboBoxModel<Hackathon> modelCombo = new DefaultComboBoxModel<>();
            for (Hackathon hacka : hackathons) {
                modelCombo.addElement(hacka);
            }
            // set DefaultComboBoxModel to ComboBox
            cmb_hackathons.setModel(modelCombo);

        } else {
            // Data recovery error
            this.lbl_error.setText("Data loading problem ");
            this.lbl_error.setVisible(true);
            this.btn_manage.setEnabled(false);
        }
        pnl_bottom.setPreferredSize(new Dimension(300, 10));
        pnl_window.add(pnl_bottom);
    }

    /**
     * METHODS RELATED TO EVENT MANAGEMENT
     */

    /**
     * To quit the application
     * Called when Close's button is clicked
     */
    private void closeButtonActionPerformed(ActionEvent e) {
        System.exit(0);

    }

    /**
     * To mamage the selected hackathon
     * Called when Manage's button is clicked
     */
    private void manageButtonActionPerformed(ActionEvent e) {

        // gets selected hackathon
        Hackathon hackathonSelected = (Hackathon) cmb_hackathons.getSelectedItem();

        // Créer une instance de la fenêtre de l'autre dossier
        EditeHackat editeHackat = new EditeHackat(hackathonSelected);

        // Afficher la fenêtre de l'autre dossier
        editeHackat.setVisible(true);

        // log
        System.out.println(hackathonSelected);
    }

    public static Hackathon toAnotherWin() {
        var hackathon = (Hackathon) cmb_hackathons.getSelectedItem();
        return hackathon;
    }

    /**
     * Add a new hackathon
     * 
     */
    private void addButtonActionperformed(ActionEvent e) {
        super.dispose();
        AddHackathon addHackathon = new AddHackathon();
        addHackathon.setVisible(true);

    }

    private void animButtonActionperformed(ActionEvent e) {
        ManageAnim manageAnim = new ManageAnim();
        manageAnim.setVisible(true);
    }

}
