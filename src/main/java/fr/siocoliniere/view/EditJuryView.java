package fr.siocoliniere.view;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Member;

public class EditJuryView extends JFrame{
    
    private JPanel pnl_main;
    private JTextField txt_firstname;
    private JTextField txt_lastname;
    private JButton btn_save;
    private JPanel pnl_textField;
    private Member member;

    public  EditJuryView(){
        var pnl_main = new JPanel();
        var txt_firstname = new JTextField();
        var txt_lastname = new JTextField();
        var btn_save = new JButton();
        var pnl_textField = new JPanel();
        var member = ManageJury.getInstance().getSelectedMember();

        //this
        var contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        //pnl_main
        {
            pnl_main.setPreferredSize(new Dimension(150, 100));
            pnl_main.setLayout(new GridLayout(2,1));
            
            //pnl_textField
            {
                pnl_textField.setLayout(new GridLayout(1,2));

                //txt_firstname
                txt_firstname.setText(member.getFirstName());
                pnl_textField.add(txt_firstname);

                //txt_lastname
                txt_lastname.setText(member.getLastName());
                pnl_textField.add(txt_lastname);
            }
            pnl_main.add(pnl_textField);

            //btn_save
            btn_save.setText("Save");
            btn_save.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    member.setFirstName(txt_firstname.getText());
                    member.setLastName(txt_lastname.getText());
                    HackathonController.getInstance().updateJuryMember(member);
                    ManageJury.getInstance().setVisible(false);
                    new ManageJury().setVisible(true);
                    EditJuryView.this.setVisible(false);
                }
                
            });
            pnl_main.add(btn_save);

        }
        contentPane.add(pnl_main);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Edit jury");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(300, 150));
    }
}
