package fr.siocoliniere.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

public class AddExpertView extends JFrame{
    private JPanel pnl_main;
    private JList<Member> lst_member;
    private JScrollPane scr_memberList;
    private JButton btn_add;
    private Hackathon hackathon;
    private JTextPane txp_domaine;
    private JPanel pnl_top;

    public AddExpertView(){
        var pnl_top = new JPanel();
        var pnl_main = new JPanel();
        var lst_member = new JList<Member>();
        var btn_add = new JButton();
        var txp_domaine = new JTextPane();
        hackathon=WelcomeForm.toAnotherWin();

        //this 
        var contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        //pnl_main
        {
            pnl_main.setPreferredSize(new Dimension(150, 100));
            pnl_main.setLayout(new GridLayout(2, 1));

            //pnl_top
            {
                pnl_top.setLayout(new GridLayout(1,2));
                //scr_memberList
                {
                    var ctrlHackathon=HackathonController.getInstance();
                    var memberModel = new DefaultListModel<Member>();
                    var members = ctrlHackathon.getNonJuryMembersByHackathon(hackathon.getId());
                    for(int i=0;i<members.size();i++){
                        memberModel.addElement(members.get(i));
                    }
                    lst_member.setModel(memberModel);
                    scr_memberList = new JScrollPane(lst_member);
                    pnl_top.add(scr_memberList);
                }
                //txp_domaine
                txp_domaine.setText("Enter domain");
                pnl_top.add(txp_domaine);
            }
            pnl_main.add(pnl_top);
            //btn_add
            btn_add.setText("Add");
            btn_add.setPreferredSize(new Dimension(50,70));
            btn_add.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    // TODO Auto-generated method stub
                    throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
                }

                
                
            });
            pnl_main.add(btn_add);
        }
        contentPane.add(pnl_main);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Add expert");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(300, 150));
    }

}
