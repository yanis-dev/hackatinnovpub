package fr.siocoliniere.view;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.dal.DALException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class EditeHackat extends JFrame {
    private JPanel pnl_top;
    private JPanel pnl_manage;
    private JComboBox<Hackathon> cmb_hackathons;
    private JLabel lbl_error;
    private JPanel pnl_bottom;
    private JButton btn_close;
    private JTextField txt_name;
    private JTextField txt_topic;
    private JTextField txt_description;
    private JTextField txt_date;
    private JTextField txt_lieu;
    private JTextField txt_theme;
    private JTextField txt_registration;

    private JPanel pnl_TextField;
    private Hackathon hackathon;
    private String editedName;
    private String editedTopic;
    private String editedDescription;
    private LocalDate editedDate;
    private String editedLieu;
    private String editedTheme;
    private String editedRegistration;

    /**
     * Constructor
     */

    public EditeHackat(Hackathon hackathon){
        pnl_top = new JPanel(null);
        pnl_manage = new JPanel();
        cmb_hackathons = new JComboBox<Hackathon>();
        lbl_error = new JLabel();
        pnl_bottom = new JPanel(null);
        btn_close = new JButton();
        this.hackathon = hackathon;
        txt_name = new JTextField();
        txt_topic = new JTextField();
        txt_description = new JTextField();
        txt_date = new JTextField();
        txt_lieu = new JTextField();
        txt_theme = new JTextField();
        txt_registration = new JTextField();

        //========this========
        var contentPane = getContentPane();
        contentPane.setLayout(new GridLayout(3, 1));

        //----pnl_top----
        pnl_top.setPreferredSize(new Dimension(10,60));
        contentPane.add(pnl_top);


        //======== pnl_manage ========
        {
            pnl_manage.setLayout(new FlowLayout(FlowLayout.TRAILING,5,20));

            //---- pnl_bottom ----
            pnl_bottom.setPreferredSize(new Dimension(300,10));
            pnl_manage.add(pnl_bottom);

            //---- btn_close ----
            btn_close.setText("Close");
            btn_close.setPreferredSize(new Dimension(100,30));
            btn_close.addActionListener(this::closeButtonActionPerformed);
            pnl_manage.add(btn_close);

            //créer un Jpanel pour contenir les JTextFields
            JPanel pnl_textField = new JPanel(new GridLayout(3, 1));

            //JTextField
            JTextField txt_name = new JTextField(hackathon.getName());
            JTextField txt_topic = new JTextField(hackathon.getTopic());
            JTextField txt_description = new JTextField(hackathon.getDescription());
            JTextField txt_date = new JTextField(String.valueOf(hackathon.getDate()));
            JTextField txt_lieu = new JTextField(hackathon.getLieu());
            JTextField txt_theme = new JTextField(hackathon.getTheme());
            JTextField txt_registration = new JTextField(hackathon.getRegistration());




            //Button Enregistrer in BDD
            JButton btn_save = new JButton("Save");

            //Ajoutez les JTextFields au JPanel
            pnl_textField.add(txt_name);
            pnl_textField.add(txt_topic);
            pnl_textField.add(txt_description);
            pnl_textField.add(txt_date);
            pnl_textField.add(txt_lieu);
            pnl_textField.add(txt_theme);
            pnl_textField.add(txt_registration);
            pnl_textField.add(btn_save);


            btn_save.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    editedName = txt_name.getText();
                    editedTopic = txt_topic.getText();
                    editedDescription = txt_description.getText();
                    try{
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        editedDate = LocalDate.parse(txt_date.getText(), formatter);
                    } catch (DateTimeParseException ex) {
                        ex.printStackTrace();
                    }
                    editedLieu = txt_lieu.getText();
                    editedTheme = txt_theme.getText();
                    editedRegistration = txt_registration.getText();
                    // Les valeurs ont été modifiées, vous pouvez les enregistrer dans la base de données

                    HackathonController.getInstance().updateHackathon(hackathon.getId(), editedName, editedTopic, editedDescription, editedDate, editedLieu, editedTheme, editedRegistration);
                    // Mise à jour réussie, afficher un message de succès
                    JOptionPane.showMessageDialog(EditeHackat.this, "Données mises à jour avec succès", "Succès", JOptionPane.INFORMATION_MESSAGE);
                }
            });

            //Ajoutez le JPanel contenant les JTextField à votre fenêtre
            getContentPane().add(pnl_textField);
        }
        contentPane.add(pnl_manage);
        pack();
        setLocationRelativeTo(getOwner());


        setTitle("manage_hackathon");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(500,500);
        setPreferredSize(new Dimension(800,200));

        lbl_error.setVisible(false);

        initComboBoxHackathons();
    }

    /**
     * Initializes the drop-down list with existing hackathons
     */

    public void initComboBoxHackathons() {
        //!!! Info technique : un objet de type ComboBox fonctionne avec un objet de type DefaultComboBoxModel qui va contenir les données

        // gets all hackathons
        List<Hackathon> hackathonList = HackathonController.getInstance().getAll();

        //create Model for Combobox
        if (hackathonList != null) {
            DefaultComboBoxModel<Hackathon> modelCombo = new DefaultComboBoxModel<>();
            for (Hackathon hacka : hackathonList) {
                modelCombo.addElement(hacka);
            }
            //set DefaultComboBoxModel to ComboBox
            cmb_hackathons.setModel(modelCombo);
        } else {
            //Data recovery error
            this.lbl_error.setText("Data loading problem ");
            this.lbl_error.setVisible(true);
        } pnl_bottom.setPreferredSize(new Dimension(300, 10));
        pnl_manage.add(pnl_bottom);
    }

    /**
     * METHODS RELATED TO EVENT MANAGEMENT
     */

    /**
     * To quit the application
     * Called when Close's button is clicked
     */

    private void closeButtonActionPerformed(ActionEvent e) {
        //System.exit(0);
        SwingUtilities.getWindowAncestor((Component)e.getSource()).dispose();
    }


}
