package fr.siocoliniere.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

public class ManageJury extends JFrame {

    private JPanel pnl_manage;
    private JPanel pnl_right;
    private JPanel pnl_left;
    private static DefaultListModel<Member> juryModel;
    private static JList<Member> lst_jury;
    private JScrollPane scr_juryList;
    private JButton btn_add;
    private JButton btn_delete;
    private Hackathon hackathon;
    private static ManageJury instanceManageJury;
    private JButton btn_edit;

    /**
     * Constructor
     */
    public ManageJury() {
        instanceManageJury = ManageJury.this;
        hackathon = WelcomeForm.toAnotherWin();
        pnl_manage = new JPanel();
        pnl_right = new JPanel();
        pnl_left = new JPanel();
        lst_jury = new JList<Member>();
        var ctrlHackathon = HackathonController.getInstance();
        juryModel = new DefaultListModel<Member>();
        var jury = ctrlHackathon.getJuriesByHackathon(hackathon.getId());
        for (int i = 0; i < jury.size(); i++) {
            juryModel.addElement(jury.get(i));
        }
        lst_jury.setModel(juryModel);
        scr_juryList = new JScrollPane(lst_jury);
        btn_add = new JButton();
        btn_delete = new JButton();
        btn_edit = new JButton();

        // this
        var contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        // pnl_manage
        {
            pnl_manage.setLayout(new GridLayout(1, 2));

            // pnl_right
            {
                pnl_right.setLayout(new FlowLayout());

                // src_juryList
                scr_juryList.setPreferredSize(new Dimension(200, 150));
                pnl_right.add(scr_juryList);

                pnl_manage.add(pnl_right);
            }

            // pnl_left
            {
                pnl_left.setLayout(new GridLayout(3, 1));

                // btn_add
                btn_add.setText("Add");
                btn_add.setMaximumSize(new Dimension(50, 30));
                btn_add.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new AddJuryView().setVisible(true);
                    }

                });
                pnl_left.add(btn_add);

                // btn_edit
                btn_edit.setText("Edit");
                btn_edit.setMaximumSize(new Dimension(50, 30));
                btn_edit.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new EditJuryView().setVisible(true);
                    }

                });
                pnl_left.add(btn_edit);
                // btn_delete
                btn_delete.setText("Delete");
                btn_delete.setMaximumSize(new Dimension(50, 30));
                btn_delete.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (lst_jury.getSelectedValue() != null) {
                            HackathonController.getInstance().DeleteJuryMember(hackathon, lst_jury.getSelectedValue());
                            ManageJury.this.setVisible(false);
                            new ManageJury().setVisible(true);
                        } else {
                            System.out.println("error");
                            JOptionPane.showMessageDialog(null, "Please select a jury member to delete");

                        }

                        new DeleteWarning().setVisible(true);
                    }

                });
                pnl_left.add(btn_delete);

                pnl_manage.add(pnl_left);
                pnl_manage.setBackground(new Color(120, 200, 168));
            }
        }
        contentPane.add(pnl_manage);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Manage jury");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(300, 150));
    }

    public static ManageJury getInstance() {
        if (instanceManageJury == null) {
            instanceManageJury = new ManageJury();
        }
        return instanceManageJury;
    }

    public Member getSelectedMember() {
        return lst_jury.getSelectedValue();
    }
}
