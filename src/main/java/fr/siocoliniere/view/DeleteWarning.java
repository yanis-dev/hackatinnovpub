package fr.siocoliniere.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;

public class DeleteWarning extends JFrame{
    private JTextField txt_warning;
    private JButton btn_cancel;
    private JButton btn_confirm;
    private JPanel pnl_main;
    private JPanel pnl_button;
    private static DeleteWarning instance;
    private Hackathon hackathon;

    public DeleteWarning(){
        var hackathon=WelcomeForm.toAnotherWin();
        var instance=DeleteWarning.this;
        var contentPane=getContentPane();
        var txt_warning=new JTextField("Do you really wish to delete that jury member ?");
        var btn_cancel=new JButton("No");
        var btn_confirm=new JButton("Yes");
        var pnl_main=new JPanel(new FlowLayout());
        var pnl_button=new JPanel(new GridLayout(1,2));
        txt_warning.setEditable(false);
        btn_cancel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteWarning.this.setVisible(false);
            }
            
        });
        btn_confirm.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteWarning.this.setVisible(false);
                HackathonController.getInstance().DeleteJuryMember(hackathon, ManageJury.getInstance().getSelectedMember());
                ManageJury.getInstance().setVisible(false);
                new ManageJury().setVisible(true);

            }
            
        });
        pnl_button.add(btn_cancel);
        pnl_button.add(btn_confirm);
        pnl_main.add(txt_warning);
        pnl_main.add(pnl_button);
        contentPane.add(pnl_main);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Warning");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(300, 150));
    }
    public static DeleteWarning  getInstance(){
        if(instance==null){
            instance=new DeleteWarning();
        }
        return instance;
    }
}
