package fr.siocoliniere.view;

import javax.swing.*;

import fr.siocoliniere.bll.HackathonController;
import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ComposeExpertTeam extends JFrame{
    private JPanel pnl_manage;
    private JPanel pnl_right;
    private JPanel pnl_left;
    private static DefaultListModel<Member> expertModel;
    private static JList<Member> lst_expert;
    private JScrollPane scr_expertList;
    private JButton btn_add;
    private JButton btn_delete;
    private Hackathon hackathon;
    private static ComposeExpertTeam instance;;
    
    public ComposeExpertTeam(){
        instance = ComposeExpertTeam.this;
        hackathon = WelcomeForm.toAnotherWin();
        pnl_manage = new JPanel();
        pnl_right = new JPanel();
        pnl_left = new JPanel();
        lst_expert = new JList<Member>();
        var ctrlHackathon = HackathonController.getInstance();
        expertModel = new DefaultListModel<Member>();


        /// !!! à changer
        var jury = ctrlHackathon.getJuriesByHackathon(hackathon.getId());
        for (int i = 0; i < jury.size(); i++) {
            expertModel.addElement(jury.get(i));
        }




        lst_expert.setModel(expertModel);
        scr_expertList = new JScrollPane(lst_expert);
        btn_add = new JButton();
        btn_delete = new JButton();

        // this
        var contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());


        // pnl_manage
        {
            pnl_manage.setLayout(new GridLayout(1, 2));

            // pnl_right
            {
                pnl_right.setLayout(new FlowLayout());

                // src_juryList
                scr_expertList.setPreferredSize(new Dimension(200, 150));
                pnl_right.add(scr_expertList);

                pnl_manage.add(pnl_right);
            }

            // pnl_left
            {
                pnl_left.setLayout(new GridLayout(2, 1));

                // btn_add
                btn_add.setText("Add");
                btn_add.setMaximumSize(new Dimension(50, 30));
                btn_add.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new AddExpertView().setVisible(true);
                    }

                });
                pnl_left.add(btn_add);

                // btn_delete
                btn_delete.setText("Delete");
                btn_delete.setMaximumSize(new Dimension(50, 30));
                btn_delete.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // TODO Auto-generated method stub
                        throw new UnsupportedOperationException("Unimplemented method 'actionPerformed'");
                    }


                });
                pnl_left.add(btn_delete);

                pnl_manage.add(pnl_left);
                pnl_manage.setBackground(new Color(120, 200, 168));
            }
        }
        contentPane.add(pnl_manage);
        pack();
        setLocationRelativeTo(getOwner());

        setTitle("Manage experts");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(500, 500);
        setPreferredSize(new Dimension(300, 150));
    }
    
}
