package fr.siocoliniere.bll;

import fr.siocoliniere.bo.Hackathon;
import fr.siocoliniere.bo.Member;
import fr.siocoliniere.dal.DALException;
import fr.siocoliniere.dal.jdbc.DAOHackathon;
import fr.siocoliniere.dal.jdbc.DAOMember;

import java.time.LocalDate;
import java.util.*;

public class HackathonController {
    private List<Hackathon> hackathons;

    /**
     * PATTERN SINGLETON : isntantiation of single class instance
     */
    private static HackathonController instanceCtrl;

    private static Map<Integer, String> registrationTypes;

    /**
     * Pattern Singleton
     * 
     * @return HackathonController
     */
    public static synchronized HackathonController getInstance() {
        if (instanceCtrl == null) {
            instanceCtrl = new HackathonController();
        }
        return instanceCtrl;
    }

    /**
     * Constructor
     * Loading the list of hackathons
     * Private : pattern Singleton
     */
    private HackathonController() {
        try {
            this.hackathons = DAOHackathon.getInstance().getAll();
        } catch (DALException e) {
            e.printStackTrace();
        }
    }

    public Map<Integer, String> getRegistrationTypes() {
        registrationTypes = new HashMap<>();
        registrationTypes.put(0, "Choisir un type");
        registrationTypes.put(1, "Libre");
        registrationTypes.put(2, "Approbation");

        return registrationTypes;
    }

    /**
     * Get all hackathons
     * 
     * @return the list of hackathons
     */
    public List<Hackathon> getAll() {
        return this.hackathons;
    }

    /**
     * Returns the list of members making up the hackathon jury whose id is passed
     * as a parameter
     * 
     * @param idHackathon : hackathon's id
     * @return List<Member>
     */
    public List<Member> getJuriesByHackathon(int idHackathon) {
        List<Member> juries = new ArrayList<>();
        for (Hackathon hacka : this.hackathons) {
            if (idHackathon == hacka.getId()) {
                juries = hacka.getJuryMembers();
            }
        }
        return juries;
    }

    public List<Member> getAnimsByHackathon(int idHackathon) {
        List<Member> anims = new ArrayList<>();
        for (Hackathon hacka : this.hackathons) {
            if (idHackathon == hacka.getId()) {
                anims = hacka.getAnimsMembers();
            }
        }
        return anims;

    }

    public void addNewHackathon(String nom, String topic, String description, LocalDate date, String lieu,
            String theme, String registration) {
        Hackathon hackathon = new Hackathon(nom, topic, description, date, lieu, theme, registration);
        this.hackathons.add(hackathon);
        DAOHackathon.getInstance().save(hackathon);

    }

    public void updateHackathon(int id, String nouveauNom, String nouveauSujet, String nouvelleDescription,
            LocalDate nouvelleDate, String nouveauLieu, String nouveauTheme, String nouvelleRegistration) {
        try {
            Hackathon hackathon = DAOHackathon.getInstance().getOneById(id);

            hackathon.setName(nouveauNom);
            hackathon.setTopic(nouveauSujet);
            hackathon.setDescription(nouvelleDescription);
            hackathon.setDate(nouvelleDate);
            hackathon.setLieu(nouveauLieu);
            hackathon.setTheme(nouveauTheme);
            hackathon.setRegistration(nouvelleRegistration);
            System.out.println(nouveauNom);
            System.out.println(nouveauSujet);
            System.out.println(nouvelleDescription);
            System.out.println(nouvelleDate);
            System.out.println(nouveauLieu);
            System.out.println(nouveauTheme);
            System.out.println(nouvelleRegistration);
            DAOHackathon.getInstance().update(hackathon);
        } catch (DALException e) {
            e.printStackTrace();
        }
    }

    public List<Member> getNonJuryMembersByHackathon(int idHackathon) {
        List<Member> members = new ArrayList<Member>();
        for (Hackathon hacka : this.hackathons) {
            if (idHackathon == hacka.getId()) {
                members = DAOMember.getInstance().getAllNonJuryByIdHackathon(hacka.getId());
            }
        }
        return members;
    }

    public List<Member> getNonAnimsMembersByHackathon(int idHackathon) {
        List<Member> members = new ArrayList<Member>();
        for (Hackathon hacka : this.hackathons) {
            if (idHackathon == hacka.getId()) {
                members = DAOMember.getInstance().getAllNonAnimsByIdHackathon(hacka.getId());
            }
        }
        return members;
    }

    public void AddJuryMember(Hackathon hackathon, Member jury) {
        DAOHackathon.getInstance().updateParticipationToJury(hackathon.getId(), jury.getId());
        hackathon.addJuryMember(jury);
    }

    public void DeleteJuryMember(Hackathon hackathon, Member jury) {
        DAOHackathon.getInstance().deleteJuryMember(hackathon.getId(), jury.getId());
        hackathon.getJuryMembers().remove(jury);
    }

    public void updateJuryMember(Member member) {
        DAOMember.getInstance().update(member);
    }

    public void AddAnimMember(Hackathon hackathon, Member anim) {
        DAOHackathon.getInstance().updateParticipationToAnim(hackathon.getId(), anim.getId());
        hackathon.addAnimMember(anim);
    }

    public void DeleteAnimMember(Hackathon hackathon, Member anim) {
        DAOHackathon.getInstance().updateParticipationToMember(hackathon.getId(), anim.getId());
        hackathon.getAnimsMembers().remove(anim);
    }

}
