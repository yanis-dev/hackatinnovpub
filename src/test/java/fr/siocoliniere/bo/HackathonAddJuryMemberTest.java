package fr.siocoliniere.bo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

class HackathonAddJuryMemberTest {
    private Hackathon hacka;

    @BeforeEach
    void init() {
        hacka = new Hackathon(1, "name1", "topic1", "desc1", LocalDate.parse("2023-12-09"), "lieu1", "theme1", "libre");
    }

    @Test
    void testAddNewJuryMember() {
        Member member = new Member(1, "lastname", "firstname");
        assertTrue(hacka.addJuryMember(member));
        assertTrue(hacka.getJuryMembers().contains(member), "Error : new member must be added ");
    }

    @Test
    void testAddExistedJuryMember() {
        Member member = new Member(1, "lastname", "firstname");
        hacka.addJuryMember(member);
        assertFalse(hacka.addJuryMember(member), "Error : member already belongs to the jury");
    }

}