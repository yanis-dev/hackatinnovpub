# HACKAT'ORGA
Projet de développement en Java sur le contexte Hackat'Innov 

## Projet Maven
*  dependance de test : junit 5
*  dependance : postgreSQL

### Externalisation de la configuration et gestion des environnements de dev et de prod

Les variables de config se trouvent dans des fichiers properties propres à chaque environnement :
* **Environnement de dev** : src/resources/dev
* **Environnement de prod** : src/resources/prod

#### Lecture des variables de config

Utilisation de la classe singleton `Config` pour lire la valeur d'une variable `maVariable` présente dans le fichier db.properties

`Config.getInstance().getProperty("maVariable");`

#### Utilisation des profiles dans le pom.xml pour gérer les deux environnements

##### Pour générer un jar avec les variables de dev (environnement par défaut) :
`mvn clean`

`mvn package` (ou `mvn package -Pdev`)

##### Pour générer un jar avec les variables de prod :
`mvn clean`

`mvn package -Pprod`


## Tests unitaires : Junit 5

##### Pour exécuter les tests unitaires

`mvn test`

## Architecture en couches:

* view : ihm

* bll : Buisness Logic Layer (Controller)

* dal : Data Access Layer (DAO)

* bo : Buisness Object (BO) - Model Class

## Documentation
* Répertoire doc 

